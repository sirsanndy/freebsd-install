# Freebsd install

# How to make EFI Bootloader

1. Check with `geom list disk`
2. Running these commands in terminal :
`mount -t msdosfs /dev/da0p1 /mnt`
`cp /boot/loader.efi /mnt/EFI/BOOT/BOOTX64.efi`
`efibootmgr -c -d /dev/ada0 -p 1 -l /EFI/BOOTX64.EFI -L "FreeBSD"`
`efibootmgr -a -b <bootnum>`

# How to install KDE5 Plasma
1. Install desktop-installer
`pkg install desktop-installer`
2. Choose KDE5 on dialog


